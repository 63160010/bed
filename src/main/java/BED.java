/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class BED {
    public static void main(String[] args) {
        Pillow pillow1 =new Pillow("Black",2);
        pillow1.print();
        System.out.println("-------");
        Bolster bolster1 =new Bolster("black",2,1);
        bolster1.print();
        System.out.println("-------");
        Blanket blanket1 =new Blanket("black");
        blanket1.print();
        System.out.println("-------");
        Bolster bolster2 =new Bolster("white",1,1);
        bolster2.print();
        System.out.println("-------");
        Pillow pillow2 =new Pillow("white",1);
        pillow2.print();
        System.out.println("-------");
    }
    
}
