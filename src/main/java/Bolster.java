/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Bolster {
    private String color;
    private int height;
    private int quantity;
    public Bolster(String color,int height,int quantity){
        this.color =color;
        this.height =height;
        this.quantity =quantity;
        System.out.println("Bolster created");
    }
    public void print(){
        System.out.println("color: "+color);
        System.out.println("height: "+height);
        System.out.println("quantity: "+quantity);
    }
}
